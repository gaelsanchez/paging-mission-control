## Requirements
To run project you will need:
- Java installed on machine
- Maven installed on machine

### Test Program

Navigate to root program folder

Now type the folling commands

```
mvn package
```
Validate, Compile and Test phases will be executed

Can also use the folling to unit test
```
mvn test
```

### Run compiled and packaged jar

The following command can be used to run the jar

```
java -jar target/Gael-Sanchez-Paging-Mission-control-0.0.1-SNAPSHOT-spring-boot-jar <Path to file with data>
```
