package com.gaelsanchez.pagingmissioncontrol;

import java.io.IOException;

import com.gaelsanchez.pagingmissioncontrol.controller.MissionController;

public class Application {

	public static void main(String[] args) throws IOException {
		
	    String filePath = args[0];
	    
	    MissionController controller = new MissionController();
	    controller.readFile(filePath);
	    controller.sendAlerts();
	    
	}

}
