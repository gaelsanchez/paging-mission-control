package com.gaelsanchez.pagingmissioncontrol.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.gaelsanchez.pagingmissioncontrol.model.AlertMessage;
import com.gaelsanchez.pagingmissioncontrol.model.LocalDateTimeSerializer;
import com.gaelsanchez.pagingmissioncontrol.model.Satellite;
import com.gaelsanchez.pagingmissioncontrol.model.TelemetryData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MissionController {
	
	private static final List<AlertMessage> alertMessages = new ArrayList<AlertMessage>();
	private final HashMap<Integer, Satellite> satellites = new HashMap<Integer, Satellite>();
	
	
	/**
	 * Adds alert object to alerts ArrayList, will be used to display violations in satellite data
	 * @param message AlertMessage object given when violation conditions are met
	 */
	public static void addAlert(AlertMessage message) {
		alertMessages.add(message);
	}
	
	public static List<AlertMessage> getAlertMessages() {
		return alertMessages;
	}
	
	public HashMap<Integer, Satellite> getSatellites(){
		return satellites;
	}
	

	/**
	 * Function that takes in path to file and reads each line.
	 * Each line is converted to TelemetryData object and monitored for violations	
	 * @param filePath	The path file being read
	 * @throws IOException	Exception thrown when no file is found
	 */
	public void readFile(String filePath) throws IOException {
		BufferedReader reader = null;
		TelemetryData inputData = null;
		String currentLine = null;
		
		// BufferedReader used to read in file
		// After file is read, reader will close for memory management
		try {
			reader = new BufferedReader(new FileReader(filePath));
			
			while((currentLine = reader.readLine()) != null) {
				inputData = pullTelemetryDataFromString(currentLine);
				monitorTelemetryData(inputData);
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(reader != null)
					reader.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	/**
	 * References stored satellites, stores new satellite if does not exist.
	 * Satellite object with then monitor for any violations
	 * @param telemetryData	Data being passed to satellite object for monitoring
	 */
	public void monitorTelemetryData(TelemetryData telemetryData) {
		Satellite satellite;
		satellite = satellites.get(telemetryData.getSatelliteId());
		
		if(satellite == null) {
			satellite = new Satellite(telemetryData.getSatelliteId());
			satellites.put(telemetryData.getSatelliteId(), satellite);
		}
		
		satellite.monitorForViololation(telemetryData);
	}
	
	
	/**
	 * Will display all alerts received when monitoring data
	 */
	public void sendAlerts() {
		GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer());
	    
	    Gson gson = gsonBuilder.setPrettyPrinting().create();
		
		System.out.println(gson.toJson(alertMessages));
	}
	
	
	/**
	 * Converts correctly formatted string input into a TelemetryData object.
	 * @param telemetryData String that will be used to create TelemetryData object
	 * @return	TelemetryData object 
	 */
	public TelemetryData pullTelemetryDataFromString(String telemetryData) {
		
		String[] data = telemetryData.split("\\|");
		DateTimeFormatter enteredFormatObj = DateTimeFormatter
				.ofPattern("yyyyMMdd HH:mm:ss.SSS");
		LocalDateTime dateAndTime = LocalDateTime.parse(data[0], enteredFormatObj);
		
		TelemetryData telemetryObj = new TelemetryData(dateAndTime,
				Integer.parseInt(data[1]),
				Double.parseDouble(data[2]),
				Double.parseDouble(data[3]),
				Double.parseDouble(data[4]),
				Double.parseDouble(data[5]),
				Double.parseDouble(data[6]),
				data[7]);
		
		return telemetryObj;
	}
	
}
