package com.gaelsanchez.pagingmissioncontrol.model;

import java.time.LocalDateTime;

public class AlertMessage {
	
	private int satelliteId;
	private String severity;
	private Component component;
	private LocalDateTime timestamp;
	
	public AlertMessage(int satelliteId, String severity, Component component, LocalDateTime timestamp) {
		super();
		this.satelliteId = satelliteId;
		this.severity = severity;
		this.component = component;
		this.timestamp = timestamp;
	}

	public int getSatelliteId() {
		return satelliteId;
	}

	public void setSatelliteId(int satelliteId) {
		this.satelliteId = satelliteId;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String serverity) {
		this.severity = serverity;
	}

	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "AlertMessage [satelliteId=" + satelliteId + ", severity=" + severity + ", component=" + component
				+ ", timestamp=" + timestamp + "]";
	}

}
