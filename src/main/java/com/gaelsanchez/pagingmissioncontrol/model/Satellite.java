package com.gaelsanchez.pagingmissioncontrol.model;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import com.gaelsanchez.pagingmissioncontrol.controller.MissionController;

public class Satellite {
	
	private final int satellite;
	
	// batteryViolations and thermostatViolations are used to store violations
	// determined by component type. Both list will never have more than three violations at
	// any time.
	private final List<TelemetryData> batteryViolations;
	private final List<TelemetryData> thermostatViolations;
	
	public Satellite(int satelliteId) {
		this.satellite = satelliteId;
		this.thermostatViolations = new ArrayList<TelemetryData>();
		this.batteryViolations = new ArrayList<TelemetryData>();
	}
	
	public int getSatelliteId() {
		return satellite;
	}

	public List<TelemetryData> getBatteryViolations() {
		return batteryViolations;
	}

	public List<TelemetryData> getThermostatViolations() {
		return thermostatViolations;
	}
	
	/**
	 * Checks for violation given component type.
	 * If violation found, add to violation specific list.
	 * @param telemetryData	Data being monitored
	 * @return	true or false value if violation is present
	 */
	public boolean monitorForViololation(TelemetryData telemetryData) {
		switch(telemetryData.getComponent()) {
			case BATT: {
					if(telemetryData.getRawValue() < telemetryData.getRedLowLimit()) {
						addViolation(telemetryData, batteryViolations);
						return true;
					}
				}
			case TSTAT: {
					if(telemetryData.getRawValue() > telemetryData.getRedHighLimit()) {
						addViolation(telemetryData, thermostatViolations);
						return true;
					}
				}
			default: 
				return false;
		}
	}
	
	/**
	 * Adds violation to specified list determined by component type. Violations will then be
	 * checked for a total of three violation and a time difference of five minutes or less.
	 * First violation will be removed from list once function is complete.
	 * @param violation	telemetry data having violated conditions
	 * @param violationsList	satellite list determined by component type
	 */
	public void addViolation(TelemetryData violation, List<TelemetryData> violationsList) {
		violationsList.add(violation);
		
		if(violationsList.size() != 3)
			return;
		
		boolean fiveMinuteInterval = violationsWithinFiveMinutes(violationsList);
		
		if(fiveMinuteInterval) {
			MissionController.addAlert(new AlertMessage(violationsList.get(0).getSatelliteId(),
					violationsList.get(0).getSeverity(),
					violationsList.get(0).getComponent(),
					violationsList.get(0).getTimeStamp())
					);
		}
		
		violationsList.remove(0);
	}
	
	/**
	 * Will take list determined by component type and calculate the difference between the first
	 * violation and the last. This will determine if the violations happened within a five minute
	 * span.
	 * @param violations	list of violations to be evaluated
	 * @return	boolean value regarding under five minutes difference
	 */
	public boolean violationsWithinFiveMinutes(List<TelemetryData> violations) {
		Duration duration = Duration.between(violations.get(0).getTimeStamp(),
				violations.get(2).getTimeStamp());
		
		if(duration.toMinutes() <= 5)
			return true;
		
		return false;
	}

	@Override
	public String toString() {
		return "Satellite [batteryViolations=" + batteryViolations
				+ ", thermostatViolations=" + thermostatViolations + "]";
	}

}
