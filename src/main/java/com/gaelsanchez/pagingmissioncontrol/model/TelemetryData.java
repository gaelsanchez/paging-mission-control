package com.gaelsanchez.pagingmissioncontrol.model;

import java.time.LocalDateTime;

public class TelemetryData {
	
	private LocalDateTime timeStamp;
	private int satelliteId;
	private double redHighLimit;
	private double yellowHighLimit;
	private double yellowLowLimit;
	private double redLowLimit;
	private double rawValue;
	private Component component;
	
	public TelemetryData(LocalDateTime timeStamp, int satelliteId, double redHighLimit, double yellowHighLimit,
			double yellowLowLimit, double redLowLimit, double rawValue, String component) {
		super();
		this.timeStamp = timeStamp;
		this.satelliteId = satelliteId;
		this.redHighLimit = redHighLimit;
		this.yellowHighLimit = yellowHighLimit;
		this.yellowLowLimit = yellowLowLimit;
		this.redLowLimit = redLowLimit;
		this.rawValue = rawValue;
		this.component = setComponent(component);
	}

	
	public TelemetryData(LocalDateTime timeStamp, int satelliteId, double redHighLimit, double yellowHighLimit,
			double yellowLowLimit, double redLowLimit, double rawValue, Component component) {
		super();
		this.timeStamp = timeStamp;
		this.satelliteId = satelliteId;
		this.redHighLimit = redHighLimit;
		this.yellowHighLimit = yellowHighLimit;
		this.yellowLowLimit = yellowLowLimit;
		this.redLowLimit = redLowLimit;
		this.rawValue = rawValue;
		this.component = component;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}

	public int getSatelliteId() {
		return satelliteId;
	}

	public void setSatelliteId(int satelliteId) {
		this.satelliteId = satelliteId;
	}

	public double getRedHighLimit() {
		return redHighLimit;
	}

	public void setRedHighLimit(double redHighLimit) {
		this.redHighLimit = redHighLimit;
	}

	public double getYellowHighLimit() {
		return yellowHighLimit;
	}

	public void setYellowHighLimit(double yellowHighLimit) {
		this.yellowHighLimit = yellowHighLimit;
	}

	public double getYellowLowLimit() {
		return yellowLowLimit;
	}

	public void setYellowLowLimit(double yellowLowLimit) {
		this.yellowLowLimit = yellowLowLimit;
	}

	public double getRedLowLimit() {
		return redLowLimit;
	}

	public void setRedLowLimit(double redLowLimit) {
		this.redLowLimit = redLowLimit;
	}

	public double getRawValue() {
		return rawValue;
	}

	public void setRawValue(double rawValue) {
		this.rawValue = rawValue;
	}

	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}
	
	public Component setComponent(String inputComponent) {
		switch(inputComponent) {
			case "BATT":
				return Component.BATT;
			case "TSTAT":
				return Component.TSTAT;
			default:
				throw new IllegalArgumentException("Invalid component type");
		}
	}
	
	public String getSeverity() {
		switch(component) {
		case BATT:
			return "RED LOW";
		case TSTAT:
			return "RED HIGH";
		default:
			return null;
		}
	}

	@Override
	public String toString() {
		return "TelemetryData [timeStamp=" + timeStamp + ", satelliteId=" + satelliteId + ", redHighLimit="
				+ redHighLimit + ", yellowHighLimit=" + yellowHighLimit + ", yellowLowLimit=" + yellowLowLimit
				+ ", redLowLimit=" + redLowLimit + ", rawValue=" + rawValue + ", component=" + component + "]";
	}
	
}
