package com.gaelsanchez.pagingmissioncontrol.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.Month;

import org.junit.Test;

import com.gaelsanchez.pagingmissioncontrol.model.Component;
import com.gaelsanchez.pagingmissioncontrol.model.Satellite;
import com.gaelsanchez.pagingmissioncontrol.model.TelemetryData;

public class MissionControllerTest {
	
	MissionController underTest;
	
	@Test
	public void itShouldAddSatelliteWhenDoesNotExist() {
		// Given
		underTest = new MissionController();
		TelemetryData data = new TelemetryData(
				LocalDateTime.now(), 1000, 101, 98, 25, 20, 102.9, Component.BATT);
		
		// When
		underTest.monitorTelemetryData(data);
		
		// Then
		assertEquals(1, underTest.getSatellites().size());
		assertEquals(1000,
			underTest.getSatellites().get(1000).getSatelliteId());
		
	}
	
	@Test
	public void itShouldNotAddNewSatelliteWhenMonitorTelemetryDataCalled() {
		// Given
		underTest = new MissionController();
		Satellite satellite = new Satellite(1000);
		TelemetryData data = new TelemetryData(
				LocalDateTime.now(), 1000, 101, 98, 25, 20, 102.9, Component.BATT);
		underTest.getSatellites().put(1000, satellite);
		
		// When
		underTest.monitorTelemetryData(data);
		
		// Then
		assertEquals(1, underTest.getSatellites().size());
		assertEquals(1000,
				underTest.getSatellites().get(1000).getSatelliteId());
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void itShouldPullTelemetryDataFromString() {
		// Given
		underTest = new MissionController();
		LocalDateTime dateTime = LocalDateTime
				.of(2020, Month.MARCH, 10, 10, 10, 10, 000);
		String telemetryData = "20200310 10:10:10.000|1000|101|25|25|25|25|BATT";
		
		TelemetryData testData = new TelemetryData(
				dateTime, 1000, 101, 25, 25, 25, 25, Component.BATT);
		
		// When
		TelemetryData pulledData = underTest
				.pullTelemetryDataFromString(telemetryData);
				
		// Then
		assertThat(pulledData).isEqualToComparingFieldByField(testData);
		
	}

}
