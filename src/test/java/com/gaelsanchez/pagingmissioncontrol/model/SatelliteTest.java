package com.gaelsanchez.pagingmissioncontrol.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.gaelsanchez.pagingmissioncontrol.controller.MissionController;

public class SatelliteTest {
	
	Satellite underTest;
	
	@Test
	public void itShouldReturnTrueWhenMonitorForViolation() {
		// Given
		underTest = new Satellite(1000);
		LocalDateTime dateAndTime = LocalDateTime.now();
		boolean monitored;
		
		TelemetryData data = new TelemetryData(
				dateAndTime, 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		
		// When
		monitored = underTest.monitorForViololation(data);
		
		// Then
		assertTrue(monitored);
		
	}
	
	@Test
	public void itShouldAddTelemetryDataToListAndReturn() {
		// Given
		underTest = new Satellite(1000);
		LocalDateTime dateAndTime = LocalDateTime.now();
		List<TelemetryData> dataList = new ArrayList<TelemetryData>();
		
		TelemetryData data = new TelemetryData(
				dateAndTime, 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		
		// When
		underTest.addViolation(data, dataList);
		
		// Then
		assertEquals(1, dataList.size());
		assertSame(data, dataList.get(0));
	}
	
	@Test
	public void itShouldSendAlertAndRemoveOneFromList() {
		// Given
		underTest = new Satellite(1000);
		LocalDateTime dateAndTime = LocalDateTime.now();
		List<TelemetryData> dataList = new ArrayList<TelemetryData>();
		
		TelemetryData data = new TelemetryData(
				dateAndTime, 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		TelemetryData dataTwo = new TelemetryData(
				dateAndTime.plusMinutes(1), 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		TelemetryData dataThree = new TelemetryData(
				dateAndTime.plusMinutes(2), 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		
		dataList.add(data);
		dataList.add(dataTwo);
		
		// When
		underTest.addViolation(dataThree, dataList);
		
		// Then
		assertEquals(1, MissionController.getAlertMessages().size());
		assertEquals(2, dataList.size());
		
	}
	
	@Test
	public void itShouldNotSendAlertAndRemoveOneFromList() {
		// Given
		underTest = new Satellite(1000);
		LocalDateTime dateAndTime = LocalDateTime.now();
		List<TelemetryData> dataList = new ArrayList<TelemetryData>();
		
		TelemetryData data = new TelemetryData(
				dateAndTime, 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		TelemetryData dataTwo = new TelemetryData(
				dateAndTime.plusMinutes(1), 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		TelemetryData dataThree = new TelemetryData(
				dateAndTime.plusMinutes(10), 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		
		dataList.add(data);
		dataList.add(dataTwo);
		
		// When
		underTest.addViolation(dataThree, dataList);
		
		// Then
		assertEquals(0, MissionController.getAlertMessages().size());
		assertEquals(2, dataList.size());
	}
	
	@Test
	public void itShouldReturnFalseWhenViolationsNotWithinFiveMinutes() {
		// Given
		boolean withinFiveMinutes;
		underTest = new Satellite(1000);
		LocalDateTime dateAndTime = LocalDateTime.now();
		List<TelemetryData> dataList = new ArrayList<TelemetryData>();
		
		TelemetryData data = new TelemetryData(
				dateAndTime, 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		TelemetryData dataTwo = new TelemetryData(
				dateAndTime.plusMinutes(1), 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		TelemetryData dataThree = new TelemetryData(
				dateAndTime.plusMinutes(10), 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		
		dataList.add(data);
		dataList.add(dataTwo);
		dataList.add(dataThree);
		
		// When
		withinFiveMinutes = underTest.violationsWithinFiveMinutes(dataList);
		
		// Then
		assertFalse(withinFiveMinutes);
	}
	
	@Test
	public void itShouldReturnTrueWhenViolationsWithinFiveMinutes() {
		// Given
		boolean withinFiveMinutes;
		underTest = new Satellite(1000);
		LocalDateTime dateAndTime = LocalDateTime.now();
		List<TelemetryData> dataList = new ArrayList<TelemetryData>();
		
		TelemetryData data = new TelemetryData(
				dateAndTime, 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		TelemetryData dataTwo = new TelemetryData(
				dateAndTime.plusMinutes(1), 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		TelemetryData dataThree = new TelemetryData(
				dateAndTime.plusMinutes(2), 1000, 101, 98, 25, 20, 102.9, "TSTAT");
		
		dataList.add(data);
		dataList.add(dataTwo);
		dataList.add(dataThree);
		
		// When
		withinFiveMinutes = underTest.violationsWithinFiveMinutes(dataList);
		
		// Then
		assertTrue(withinFiveMinutes);
	}
	

}
