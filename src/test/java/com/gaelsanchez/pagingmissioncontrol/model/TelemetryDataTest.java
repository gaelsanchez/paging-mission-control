package com.gaelsanchez.pagingmissioncontrol.model;

import static org.junit.Assert.assertThrows;

import java.time.LocalDateTime;

import org.junit.Test;

public class TelemetryDataTest {
	
	TelemetryData underTest;

	@Test
	public void itShouldThrowIllegalArgumentExceptionWhenSettingComponent() {
		// Given
		underTest = new TelemetryData(
				LocalDateTime.now(), 1000, 101, 98, 25, 20, 102.9, Component.BATT);

		// When
		// Then
		assertThrows(IllegalArgumentException.class, () -> {
			underTest.setComponent("Invalid");
		});
	}
	
}
